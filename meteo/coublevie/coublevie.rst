.. index::
   pair: Météo ; Coublevie
   ! Coublevie

.. _meteo_coublevie:

=============================================================================
**méteo Coublevie**
=============================================================================

- https://fr.wikipedia.org/wiki/Coublevie
- https://www.openstreetmap.org/#map=15/45.3571/5.6105

.. raw:: html

   <iframe width="800" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
     src="https://www.openstreetmap.org/export/embed.html?bbox=5.582170486450196%2C45.34427329049454%2C5.6388187408447275%2C45.36999635126154&amp;layer=mapnik"
     style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=15/45.3571/5.6105">Afficher une carte plus grande</a></small>

:download:`Faites un clic droit pour ouvrir un nouvel onglet et accéder à la météo de  de Coublevie <meteo_coublevie.html>`

