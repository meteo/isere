.. index::
   pair: Météo ; Voreppe
   ! Voreppe

.. _meteo_voreppe:

=============================================================================
**méteo Voreppe**
=============================================================================

- https://www.openstreetmap.org/#map=14/45.2887/5.6403

.. raw:: html

   <iframe width="800" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
       src="https://www.openstreetmap.org/export/embed.html?bbox=5.583629608154298%2C45.26395305883211%2C5.69692611694336%2C45.313408289905496&amp;layer=mapnik"
       style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=14/45.2887/5.6403">Afficher une carte plus grande</a></small>



:download:`Faites un clic droit pour ouvrir un nouvel onglet et accéder à la météo de  du Voreppe <meteo_voreppe.html>`

