.. index::
   pair: Météo ; Lans-en-Vercors
   ! La Buisse

.. _meteo_lans_en_vercors:

=============================================================================
**méteo Lans-en-Vercors**
=============================================================================

- https://fr.wikipedia.org/wiki/Lans-en-Vercors
- https://www.openstreetmap.org/#map=14/45.1261/5.5753

.. raw:: html

   <iframe width="800" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
   src="https://www.openstreetmap.org/export/embed.html?bbox=5.5036354064941415%2C45.10133544669321%2C5.646972656250001%2C45.1509322029437&amp;layer=mapnik"
   style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=14/45.1261/5.5753">Afficher une carte plus grande</a></small>


:download:`Faites un clic droit pour ouvrir un nouvel onglet et accéder à la météo de  de Lans en Vercors <meteo_lans_en_vercors.html>`

