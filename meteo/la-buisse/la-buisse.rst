.. index::
   pair: Météo ; La Buisse
   ! La Buisse

.. _meteo_la_buisse:

=============================================================================
**méteo La Buisse**
=============================================================================

- https://fr.wikipedia.org/wiki/La_Buisse
- https://www.openstreetmap.org/#map=14/45.3330/5.6335

.. raw:: html

   <iframe width="800" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
       src="https://www.openstreetmap.org/export/embed.html?bbox=5.576848983764649%2C45.30827757898557%2C5.690145492553711%2C45.35769416563272&amp;layer=mapnik"
       style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=14/45.3330/5.6335">Afficher une carte plus grande</a></small>



:download:`Faites un clic droit pour ouvrir un nouvel onglet et accéder à la météo de  de La buisse <meteo_la_buisse.html>`

