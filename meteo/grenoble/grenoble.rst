.. index::
   pair: Météo ; Grenoble
   ! Grenoble

.. _meteo_grenoble:

=============================================================================
**méteo Grenoble**
=============================================================================

- https://fr.wikipedia.org/wiki/Grenoble
- https://meteofrance.com/previsions-meteo-france/grenoble/38000


.. raw:: html

   <iframe width="800" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
       src="https://www.openstreetmap.org/export/embed.html?bbox=5.601997375488282%2C45.13458638864562%2C5.828590393066407%2C45.233678968522185&amp;layer=mapnik" style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=13/45.1842/5.7153">Afficher une carte plus grande</a></small>


:download:`Faites un clic droit pour ouvrir un nouvel onglet et accéder à la météo de  de Grenoble <meteo_grenoble.html>`
