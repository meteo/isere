.. index::
   pair: Météo ; Fontanil-cornillon
   ! Fontanil-cornillon

.. _meteo_fontanil_cornillon:

=============================================================================
**méteo Fontanil-cornillon**
=============================================================================

- https://fr.wikipedia.org/wiki/Fontanil-Cornillon
- https://www.openstreetmap.org/#map=17/45.24821/5.66285

.. raw:: html

   <iframe width="800" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
       src="https://www.openstreetmap.org/export/embed.html?bbox=5.6557703018188485%2C45.2451167363561%2C5.6699323654174805%2C45.25130305007495&amp;layer=mapnik" style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=17/45.24821/5.66285">Afficher une carte plus grande</a></small>


:download:`Faites un clic droit pour ouvrir un nouvel onglet et accéder à la météo de  de Fontanil-cornillon <meteo_fontanil_cornillon.html>`

