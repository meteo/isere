# Minimal makefile for Sphinx documentation
#

# You can set these variables from the command line.
SPHINXOPTS    =
SPHINXBUILD   = sphinx-build
SPHINXPROJ    = Intranet
SOURCEDIR     = .
BUILDDIR      = _build

THIS_MAKEFILE := $(lastword $(MAKEFILE_LIST))

# Put it first so that "make" without argument is like "make help".
help:
	@$(SPHINXBUILD) -M help "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)
	@echo " "
	@echo "Targets:"
	@echo " "
	@echo "- make check_all"
	@echo "- make req"
	@echo "- make update"
	@echo "- make clearcache"
	@echo " "


check_all:
	pre-commit run --all-files

clearcache:
	poetry cache clear --all pypi

req:
	poetry env info --path
	poetry show --tree
	poetry check
	poetry export -f requirements.txt --without-hashes  > requirements.txt
	cat requirements.txt

update:
	poetry update
	@$(MAKE) -f $(THIS_MAKEFILE) req
	git diff requirements.txt
	pre-commit autoupdate

api:
	sphinx-apidoc -o src/articles src/articles
	#rm src/src.sorl.*
	#rm src/src.services.*
	#rm src/src.tiers.*


.PHONY: help Makefile

# Catch-all target: route all unknown targets to Sphinx using the new
# "make mode" option.  $(O) is meant as a shortcut for $(SPHINXOPTS).
%: Makefile
	@$(SPHINXBUILD) -M $@ "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)
