# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# http://www.sphinx-doc.org/en/master/config
# -- Path setup --------------------------------------------------------------
# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))
#
# Modeles de fichiers conf.py
# ============================
#
# - https://github.com/bashtage/sphinx-material/blob/master/docs/conf.py
# - https://github.com/bashtage/arch/blob/main/doc/source/conf.py
# - https://github.com/statsmodels/statsmodels/blob/main/docs/source/conf.py
#
# theme.conf
# ===========
# - https://github.com/bashtage/sphinx-material/blob/master/sphinx_material/sphinx_material/theme.conf
#
import platform
from datetime import datetime
from zoneinfo import ZoneInfo

import sphinx
import sphinx_material

project = "Météo Isère"
html_title = project
html_short_title = project
author = "Noam"
html_logo = "images/meteo_avatar.png"
html_favicon = "images/meteo_avatar.png"
now = datetime.now(tz=ZoneInfo("Europe/Paris"))
master_doc = "index"
version = "0.1.0"
release = version
today = f"{now.year}-{now.month:02}-{now.day:02} {now.hour:02}H ({now.tzinfo})"
source_suffix = ".rst"
language = None
exclude_patterns = ["_build", ".venv", ".git"]

extensions = [
    "sphinx.ext.extlinks",
    "sphinx.ext.intersphinx",
    "sphinx.ext.todo",
    "sphinx.ext.viewcode",
    "sphinx_copybutton",
]


# https://sphinx-design.readthedocs.io/en/furo-theme/get_started.html
extensions.append("sphinx_design")


# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]
html_static_path = ["_static"]
html_show_sourcelink = True
html_sidebars = {
    "**": ["logo-text.html", "globaltoc.html", "localtoc.html", "searchbox.html"]
}
extensions.append("sphinx_material")
html_theme_path = sphinx_material.html_theme_path()
html_context = sphinx_material.get_html_context()
html_theme = "sphinx_material"

extensions.append("sphinx.ext.intersphinx")
intersphinx_mapping = {
    "demo_sphinx_furo": (
        "https://informatique.gitlab.srv.int.id3.eu/demos-sphinx/demo-sphinx-furo/",
        None,
    ),
    "demo_bookmarks": (
        "https://informatique.gitlab.srv.int.id3.eu/demos-sphinx/bookmarks",
        None,
    ),
}
# python -m sphinx.ext.intersphinx http://informatique.gitlab.srv.int.id3.eu/Documentation/objects.inv
# python -m sphinx.ext.intersphinx https://informatique.gitlab.srv.int.id3.eu/intranet/intranet_user/objects.inv
# python -m sphinx.ext.intersphinx https://docs.ray.io/en/latest/objects.inv
# python -m sphinx.ext.intersphinx https://www.sphinx-doc.org/en/master/objects.inv
# python -m sphinx.ext.intersphinx https://informatique.gitlab.srv.int.id3.eu/demos-sphinx/demo-sphinx-furo/objects.inv
# python -m sphinx.ext.intersphinx https://informatique.gitlab.srv.int.id3.eu/demos-sphinx/bookmarks/objects.inv
# python -m sphinx.ext.intersphinx https://meteo.frama.io/isere/objects.inv
extensions.append("sphinx.ext.todo")
todo_include_todos = True

# material theme options (see theme.conf for more information)
# https://github.com/bashtage/sphinx-material/blob/master/sphinx_material/sphinx_material/theme.conf
# Colors
# The theme color for mobile browsers. Hex color.
# theme_color = #3f51b5
# Primary colors:
# red, pink, purple, deep-purple, indigo, blue, light-blue, cyan,
# teal, green, light-green, lime, yellow, amber, orange, deep-orange,
# brown, grey, blue-grey, white
# Accent colors:
# red, pink, purple, deep-purple, indigo, blue, light-blue, cyan,
# teal, green, light-green, lime, yellow, amber, orange, deep-orange
# color_accent = blue
# color_primary = blue-grey

html_theme_options = {
    "base_url": "https://meteo.frama.io/isere/",
    "repo_url": "https://framagit.org/meteo/isere",
    "repo_name": project,
    "repo_type": "gitlab",
    "html_minify": False,
    "html_prettify": False,
    "css_minify": False,
    "globaltoc_depth": -1,
    # https://www.colorhexa.com/2173f3
    "theme_color": "#a121f3",
    "color_primary": "indigo",
    "color_accent": "blue",
    "master_doc": False,
    "nav_title": f"{project} {release} ({today})",
    "nav_links": [
        {
            "href": "genindex",
            "internal": True,
            "title": "Index",
        },
                {
            "href": "https://meteo.frama.io/isere/_downloads/f0a426be573ddf713f059e521f1c4c43/meteo_coublevie.html",
            "internal": False,
            "title": "Coublevie",
        },
        {
            "href": "https://meteo.frama.io/isere/_downloads/c9f264087e2947152332af4ddac8bf35/meteo_fontanil_cornillon.html",
            "internal": False,
            "title": "Fontanil-Cornillon",
        },
        {
            "href": "https://meteo.frama.io/isere/_downloads/927f6e1cfb0209ad3be2d391449d20c1/meteo_grenoble.html",
            "internal": False,
            "title": "Grenoble",
        },
        {
            "href": "https://meteo.frama.io/isere/_downloads/81d762a7015cb61b6eb15eef95926c45/meteo_la_buisse.html",
            "internal": False,
            "title": "La Buisse",
        },
        {
            "href": "https://meteo.frama.io/isere/_downloads/65d3c67a16a533bc8a64e4a825d413f3/meteo_lans_en_vercors.html",
            "internal": False,
            "title": "Lans-en-Vercors",
        },
        {
            "href": "https://meteo.frama.io/isere/_downloads/8afe789f332f3e9a27edf30f5ee38800/meteo_saint_hilaire.html",
            "internal": False,
            "title": "Saint Hilaire du Touvet",
        },
                        {
            "href": "https://meteo.frama.io/isere/_downloads/02217dff95b0927e6a6b9347ddafef6b/meteo_saint_pierre_de_chartreuse.html",
            "internal": False,
            "title": "Saint Pierre de Chartreuse",
        },
        {
            "href": "https://meteo.frama.io/isere/_downloads/7b553901219d87bc749ccf9e341f9c14/meteo_voreppe.html",
            "internal": False,
            "title": "Voreppe",
        },
    ],
    "heroes": {
        "index": "Météo Isère",
    },
    "table_classes": ["plain"],
}

##################################################################

language = "fr"
html_last_updated_fmt = ""
todo_include_todos = True

html_use_index = True
html_domain_indices = True

html_static_path = ["_static"]
html_css_files = ["custom.css"]


copyright = f"2023-{now.year}, {author} Built with sphinx {sphinx.__version__} Python {platform.python_version()}"

# en général les images avatar font 64x64
rst_prolog = """
"""
